package main

import (
	"bytes"
	"encoding/json"
	"net/http"
	"testing"

	"go.mongodb.org/mongo-driver/bson/primitive"
)

type InsertedStructure struct {
	ID string `json:"InsertedID"`
}

type PersonLTD struct {
	ID        primitive.ObjectID `json:"_id,omitempty" bson:"_id,omitempty"`
	Firstname string             `json:"firstname,omitempty" bson:"firstname,omitempty"`
	Lastname  string             `json:"lastname,omitempty" bson:"lastname,omitempty"`
}

func TestIntegrationWithMongoDb(t *testing.T) {
	data := []byte(`{"firstname":"anosov", "lastname": "konstantin"}`)
	r := bytes.NewReader(data)
	resp, err := http.Post("http://kubitre_test_api_service:9999/person", "application/json", r)
	if err != nil {
		t.Error(err)
	}
	t.Log("Complete created user")
	var people InsertedStructure
	defer resp.Body.Close()
	if err := json.NewDecoder(resp.Body).Decode(&people); err != nil {
		t.Error(err)
	}
	t.Log("User id: ", people.ID)
	resp2, err2 := http.Get("http://kubitre_test_api_service:9999/person/" + people.ID)
	if err2 != nil {
		t.Error(err2)
	}
	var peopleCreated []PersonLTD
	if err3 := json.NewDecoder(resp2.Body).Decode(&peopleCreated); err3 != nil {
		t.Error(err3)
	}
	if peopleCreated[0].Firstname == "anosov" && peopleCreated[0].Lastname == "konstantin" {
		t.Log("successful created one person")
	} else {
		t.Error("Can not create user")
	}
}
