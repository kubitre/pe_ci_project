FROM golang:1.13.1-alpine3.10 as builder

WORKDIR /app

ADD go.mod .
ADD go.sum .

RUN go mod download

ADD main.go .

RUN GOOS=linux GOARCH=amd64 CGO_ENABLED=0 go build -o test_api.bin -ldflags="-w -s" main.go

FROM scratch
COPY --from=builder /app/test_api.bin /usr/bin/test_api.bin
ENTRYPOINT ["/usr/bin/test_api.bin"]