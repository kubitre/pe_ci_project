package main

import (
	"context"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"time"

	"github.com/gorilla/mux"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

var client *mongo.Client

const (
	collection = "konstantin_ansoov_collection"
	peopleColl = "people"
)

type Person struct {
	ID        primitive.ObjectID `json:"_id,omitempty" bson:"_id,omitempty"`
	Firstname string             `json:"firstname,omitempty" bson:"firstname,omitempty"`
	Lastname  string             `json:"lastname,omitempty" bson:"lastname,omitempty"`
}

func CreatePersonEndpoint(response http.ResponseWriter, request *http.Request) {
	response.Header().Set("content-type", "application/json")
	var person Person
	err3 := json.NewDecoder(request.Body).Decode(&person)
	if err3 != nil {
		log.Panic(err3)
	}
	collection := client.Database(collection).Collection(peopleColl)
	ctx, err2 := context.WithTimeout(context.Background(), 5*time.Second)
	if err2 != nil {
		log.Panic(err2)
	}
	result, err4 := collection.InsertOne(ctx, person)
	if err4 != nil {
		log.Panic(err4)
	}
	if err5 := json.NewEncoder(response).Encode(result); err5 != nil {
		log.Panic(err5)
	}
}
func GetPeopleEndpoint(response http.ResponseWriter, request *http.Request) {
	response.Header().Set("content-type", "application/json")
	params := mux.Vars(request)
	id, err2 := primitive.ObjectIDFromHex(params["id"])
	if err2 != nil {
		log.Panic(err2)
	}
	var person Person
	collection := client.Database(collection).Collection(peopleColl)
	ctx, err3 := context.WithTimeout(context.Background(), 30*time.Second)
	if err3 != nil {
		log.Panic(err3)
	}
	err := collection.FindOne(ctx, Person{ID: id}).Decode(&person)
	if err != nil {
		response.WriteHeader(http.StatusInternalServerError)
		status, err4 := response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		if err4 != nil {
			log.Panic(err4)
		}
		log.Println(status)
		return
	}
	if err6 := json.NewEncoder(response).Encode(person); err6 != nil {
		log.Panic(err6)
	}
}
func GetPersonEndpoint(response http.ResponseWriter, request *http.Request) {
	response.Header().Set("content-type", "application/json")
	var people []Person
	collection := client.Database(collection).Collection(peopleColl)
	ctx, err2 := context.WithTimeout(context.Background(), 30*time.Second)
	if err2 != nil {
		log.Panic(err2)
	}
	cursor, err := collection.Find(ctx, bson.M{})
	if err != nil {
		response.WriteHeader(http.StatusInternalServerError)
		status, err4 := response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		if err4 != nil {
			log.Panic(err4)
		}
		log.Println(status)
		return
	}
	defer cursor.Close(ctx)
	for cursor.Next(ctx) {
		var person Person
		if err7 := cursor.Decode(&person); err7 != nil {
			log.Panic(err7)
		}
		people = append(people, person)
	}
	last(response, cursor, people)
}

func last(response http.ResponseWriter, cursor *mongo.Cursor, people []Person) {
	if err := cursor.Err(); err != nil {
		response.WriteHeader(http.StatusInternalServerError)
		status, err3 := response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		if err3 != nil {
			log.Panic(err3)
		}
		log.Println(status)
		return
	}
	if err2 := json.NewEncoder(response).Encode(people); err2 != nil {
		log.Panic(err2)
	}
}

func main() {
	fmt.Println("Starting the application...")
	ctx, err := context.WithTimeout(context.Background(), 10*time.Second)
	if err != nil {
		log.Panic(err)
	}
	clientOptions := options.Client().ApplyURI("mongodb://mongodb_service:27017")
	client, _ = mongo.Connect(ctx, clientOptions)
	router := mux.NewRouter()
	router.HandleFunc("/person", CreatePersonEndpoint).Methods("POST")
	router.HandleFunc("/people", GetPeopleEndpoint).Methods("GET")
	router.HandleFunc("/person/{id}", GetPersonEndpoint).Methods("GET")
	if err8 := http.ListenAndServe(":9999", router); err8 != nil {
		log.Panic(err8)
	}
}
